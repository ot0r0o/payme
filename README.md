
Verse Test: Payme
=============

This code is a Kotlin solution for the Verse test. It has been developed with:

- Kotlin 1.2.41

- Spring Boot 2.1

- Gradle


What it offers?
-----
This is a very small solution of the original Verse environment. What offers to the users?

- Create new user.

- Edit an user.

- Check his current balance.

- List his latest movements (incoming & outgoing).

- Send money to another user (with currency exchange).

- Add money to increase balance.

*ADMIN ONLY*:

- Check balance of any user.

- List all the users registered in the platform.

Compile & Run
-----
Before compile and run the server, there are some requeriments:

- Java 8
>- $ sudo apt install openjdk-8-jdk
>- $ java -version

- Docker
>- $ sudo apt install docker.io
>- $ docker --version

- Docker Compose
>- $ sudo apt install docker-compose
>- $ docker-compose --version

Compile & Run on UNIX system:
> $ ./gradlew clean build

After testing and compilation, an executable JAR (payme.jar) can be found in *build/libs* folder. Next step, run it in Docker.

> $ docker-compose up

Docker compose will create an image for the server and another for the DB (MySQL 5.7).

Let's use it
------
The server offers a REST API that can be used adn tested through web thanks to SwaggerUI: http://localhost:8080/swagger-ui.html#/

* auth-controller
>- ../auth/token: This endpoint is used to be authenticated in the service thanks to the username and password. The server will return a JWT used to be authorized in the rest of endpoints.
* admin-controller
>- ../admin/b/{username}: Get balance of an user
>- ../admin/all: Lists all the users registered in the platform
* balance-controller
>- ../api/balance: Get the current balance
>- ../api/balance/add/{money}: Add more money to the current balance
>- ../api/balance/movements: Returns the last 10 movements and paginated
>- ../api/balance/movements: Same, but now we can decide the page and the maximum movements each page
>- ../api/balance/tx: Send money to other users
* user-controller
>- ../api/user/create: Used to register a new user in the platform
>- ../api/user/edit: Edits user entity

There are 2 testing users, *user1:1234* with admin role and *user2:4321* as standard user. Once obtained the token, use *Authorize* button and paste in this way: Bearer $TOKEN.

Another way to obtain token:
> curl -X POST --user 'paymejwtclient:1234' -d 'grant_type=password&username=user1&password=1234' http://localhost:8080/oauth/token