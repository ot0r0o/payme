package dev.toro.payme.service

import dev.toro.payme.PaymeApplication
import org.junit.Before
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [PaymeApplication::class])
@ActiveProfiles("test")
@ConfigurationProperties("application-test.yaml")
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
@Ignore
class ServiceTests {
    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var balanceService: BalanceService

    @Before
    fun setUp() {
//        userService.createUser(
//                UserDTO("u1", "u", "1", 100, "EUR", "1"))
//        userService.createUser(
//                UserDTO("u2", "u", "2", 10, "EUR", "2"))
    }
}
