package dev.toro.payme.service

import dev.toro.payme.controller.domain.UserDTO
import org.junit.Assert
import org.junit.Test
import org.springframework.security.core.userdetails.UsernameNotFoundException

class UserServiceTest: ServiceTests() {

    @Test
    fun findByUserName() {
        Assert.assertEquals("Doe", userService.findByUserName("user1").lastName)
    }

    @Test(expected = UsernameNotFoundException::class)
    fun findUserNotFound_fail() {
        userService.findByUserName("u3")
    }

    @Test
    fun createUser() {
        userService.createUser(UserDTO("u4","u", "4", 0.0, "USD", "4"), false)
        Thread.sleep(500)
        Assert.assertEquals(0, userService.findByUserName("u4").balance.toInt())
    }

    @Test
    fun editUser() {
        userService.editUser(UserDTO("user1","UU", "1", 1000.0, "USD", "4"))
        Assert.assertEquals("UU", userService.findByUserName("user1").firstName)
        Assert.assertEquals(1000, userService.findByUserName("user1").balance.toInt())
        Assert.assertEquals("USD", userService.findByUserName("user1").currency.name)
    }
}