package dev.toro.payme.service

import dev.toro.payme.controller.exception.MoneyTransferException
import org.junit.Assert
import org.junit.Test
import org.springframework.data.domain.PageRequest
import org.springframework.security.core.userdetails.UsernameNotFoundException

class BalanceServiceTest: ServiceTests() {
    
    @Test
    fun sendMoneySuccessful() {
        Assert.assertEquals("5.0€", balanceService.transfer("user1", "user2", 5.0, "test"))
        Assert.assertEquals(1, balanceService.getTransactions("user2", PageRequest.of(0,10)).totalElements)
    }

    @Test(expected = MoneyTransferException::class)
    fun sendMoneyNotEnoughAmount_fail() {
        balanceService.transfer("user2", "user1", 50.0, "test")
    }

    @Test(expected = UsernameNotFoundException::class)
    fun sendMoneyUserNotFound_fail() {
        balanceService.transfer("u3", "user1", 50.0, "test")
    }

    @Test
    fun currentBalance() {
        Assert.assertEquals("30.0$", balanceService.currentBalance("user2"))
    }

    @Test(expected = UsernameNotFoundException::class)
    fun currentBalanceUserNotFound_fail() {
        Assert.assertEquals("10.0€", balanceService.currentBalance("u3"))
    }

    @Test
    fun addMoney() {
        Assert.assertEquals("130.0$", balanceService.addMoney("user2", 100))
    }

    @Test(expected = UsernameNotFoundException::class)
    fun addMoneyUserNotFound_fail() {
        balanceService.addMoney("u3", 100)
    }
}