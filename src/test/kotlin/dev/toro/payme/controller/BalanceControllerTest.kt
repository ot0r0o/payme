package dev.toro.payme.controller

import dev.toro.payme.controller.domain.TxDTO
import dev.toro.payme.payme.ControllerTests
import org.junit.Assert
import org.junit.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.springframework.http.*
import org.skyscreamer.jsonassert.RegularExpressionValueMatcher
import org.skyscreamer.jsonassert.Customization
import org.skyscreamer.jsonassert.JSONCompareMode
import org.skyscreamer.jsonassert.comparator.CustomComparator
import org.json.JSONException



class BalanceControllerTest: ControllerTests() {

    var context = "/api/balance"

    @Test
    fun testAddMoney() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        val entity: HttpEntity<String> = HttpEntity(null,  headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/add/10"),
                        HttpMethod.GET, entity, String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        doTest("new_balance", "(\\d+(?:[.,]\\d+)).","{\"new_balance\":x}", response.body)

    }

    @Test
    fun testAddMoney_negative_fail() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(null,  headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/add/-10"),
                        HttpMethod.GET, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }

    @Test
    fun testGetBalance() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(null,  headers)

        val response = testRestTemplate
                .exchange(createURLWithPort(context),
                        HttpMethod.GET, entity, String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        doTest("current_balance", "(\\d+(?:[.,]\\d+)).","{\"current_balance\":x}", response.body)
    }

    @Test
    fun testSendMoney() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(TxDTO("user2", 5.0, "TEST!!!")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/tx"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        doTest("new_balance", "(\\d+(?:[.,]\\d+)).","{\"new_balance\":x}", response.body)
    }

    @Test
    fun testSendMoney_usernotfound_fail() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(TxDTO("user6", 5.0, "TEST!!!")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/tx"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.NOT_FOUND, response.statusCode)
    }

    @Test
    fun testMovements() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(TxDTO("user2", 5.0, "TEST!!!")), headers)

        testRestTemplate
                .exchange(createURLWithPort("$context/tx"),
                        HttpMethod.POST, entity, String::class.java)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/movements"),
                        HttpMethod.GET, HttpEntity(null, headers), String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        Assert.assertTrue(response.body!!.contains("\"msg\":\"TEST!!!"))
    }

    @Test
    fun testMovements_notenoughbalance_fail() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(TxDTO("user2", 100.0, "TEST!!!")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/tx"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }

    @Throws(JSONException::class)
    private fun doTest(jsonPath: String, regex: String, expectedJSON: String,
                       actualJSON: String?) {
        JSONAssert.assertEquals(expectedJSON, actualJSON, CustomComparator(
                JSONCompareMode.STRICT_ORDER, Customization(jsonPath,
                RegularExpressionValueMatcher(regex))))
    }
}