package dev.toro.payme.controller

import dev.toro.payme.payme.ControllerTests
import org.junit.Assert
import org.junit.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus

class AdminControllerTest: ControllerTests() {

    var context = "/admin"

    @Test
    fun testCheckAllUsers() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        val entity: HttpEntity<String> = HttpEntity(null, headers)
        val response = testRestTemplate
                .exchange(createURLWithPort("$context/u/all"),
                        HttpMethod.GET, entity, String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        Assert.assertTrue(!objectMapper.readValue(response.body, List::class.java).isEmpty())
    }

    @Test
    fun testCheckBalance() {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $token")
        val entity: HttpEntity<String> = HttpEntity(null, headers)
        val response = testRestTemplate
                .exchange(createURLWithPort("$context/b/user1"),
                        HttpMethod.GET, entity, String::class.java)

        Assert.assertEquals(HttpStatus.OK, response.statusCode)
        JSONAssert.assertEquals("{\"current_balance\":10.0€}", response.body, true)
    }
}