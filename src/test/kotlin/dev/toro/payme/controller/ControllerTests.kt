package dev.toro.payme.payme

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.toro.payme.controller.domain.Credentials
import org.junit.Assert
import org.junit.Before
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.net.URI

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@ConfigurationProperties("application-test.yaml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Ignore
class ControllerTests {

    val objectMapper = jacksonObjectMapper()
    val host = "http://localhost"

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate
    @LocalServerPort
    var port: Int = 0

    lateinit var token: String

    @Before
    fun setUp() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(Credentials("user1", "1234")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("/auth/token"),
                        HttpMethod.POST, entity, String::class.java)
        val oauthToken: OAuth2AccessToken = objectMapper.readValue(response.body!!)
        Assert.assertEquals("bearer", oauthToken.tokenType)
        token = oauthToken.value
    }

    protected fun createURLWithPort(uri: String): URI? {
        return URI.create("$host:$port$uri")
    }
}
