package dev.toro.payme.controller

import dev.toro.payme.controller.domain.UserDTO
import dev.toro.payme.payme.ControllerTests
import org.junit.Assert
import org.junit.Test
import org.springframework.http.*
import java.net.URI

class UserControllerTest: ControllerTests() {

    var context = "/api/user"

    @Test
    fun testCreateUser() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(UserDTO("new_user", "New", "1", 1.0, "EUR", "1234")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.CREATED, response.statusCode)
    }

    @Test
    fun testCreateUser_duplicate_fail() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(UserDTO("new_dup_user", "New", "1", 1.0, "EUR", "1234")), headers)

        testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }

    @Test
    fun testCreateUser_emptyusername_fail() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(UserDTO("", "New", "1", 1.0, "EUR", "1234")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }

    @Test
    fun testCreateUser_shortpassword_fail() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(UserDTO("user", "New", "1", 1.0, "EUR", "1")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }

    @Test
    fun testCreateUser_unkowncurrency_fail() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val entity: HttpEntity<String> = HttpEntity(
                objectMapper.writeValueAsString(UserDTO("user", "New", "1", 1.0, "ASDASD", "1234")), headers)

        val response = testRestTemplate
                .exchange(createURLWithPort("$context/create"),
                        HttpMethod.POST, entity, String::class.java)

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.statusCode)
    }
}