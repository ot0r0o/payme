package dev.toro.payme.controller

import dev.toro.payme.controller.domain.TxDTO
import dev.toro.payme.repository.entity.Transaction
import dev.toro.payme.service.BalanceService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/balance")
class BalanceController(val service: BalanceService) {

    /**
     * This endpoint returns the current balance
     */
    @GetMapping()
    fun getBalance() =
            ResponseEntity.ok().body(hashMapOf("current_balance" to
                    service.currentBalance(SecurityContextHolder.getContext().authentication.principal.toString())))

    /**
     * This endpoint adds money to the user's balance
     */
    @GetMapping("/add/{money}")
    fun addMoney(@PathVariable("money") money: Int): ResponseEntity<HashMap<String, String>> =
            ResponseEntity.ok().body(hashMapOf("new_balance" to service.addMoney(
                    SecurityContextHolder.getContext().authentication.principal.toString(), money)))

    /**
     * This endpoint sends money to a specified user
     */
    @PostMapping("/tx")
    fun sendMoney(@RequestBody tx: TxDTO): ResponseEntity<HashMap<String, String>> =
            ResponseEntity.ok().body(hashMapOf("new_balance" to service.transfer(
                    SecurityContextHolder.getContext().authentication.principal.toString(),
                    tx.destinationUser, tx.amount, tx.msg)))

    /**
     * This endpoint returns the last movements. Page and total movements per page can be specified
     */
    @GetMapping("/movements/{page}/{max}")
    fun lastMovements(@PathVariable(value = "page", required = false) page: Int = 0,
                      @PathVariable(value = "max", required = false) max: Int = 10): ResponseEntity<Page<Transaction>> =
            ResponseEntity.ok().body(service.getTransactions(
                    SecurityContextHolder.getContext().authentication.principal.toString(),
                    PageRequest.of(page, max)))

    /**
     * This endpoint returns the last 10 movements (paginated)
     */
    @GetMapping("/movements")
    fun lastMovements(): ResponseEntity<Page<Transaction>> =
            ResponseEntity.ok().body(service.getTransactions(
                    SecurityContextHolder.getContext().authentication.principal.toString(),
                    PageRequest.of(0, 10)))

}