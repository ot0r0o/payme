package dev.toro.payme.controller.domain

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class Credentials(
        @field:NotNull(message = "Username can't be missing")
        @field:Size(min = 1, message = "Username can't be empty")
        val userName: String,
        @field:NotNull(message = "Password can't be missing")
        @field:Size(min = 4, message = "Password can't be short")
        val password: String
)


