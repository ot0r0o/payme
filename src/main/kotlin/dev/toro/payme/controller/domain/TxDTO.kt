package dev.toro.payme.controller.domain

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class TxDTO(
        @field:NotNull(message = "Destination user can't be missing")
        @field:Size(min = 1, message = "Destination user can't be empty")
        val destinationUser: String,
        @field:NotNull(message = "Amount can't be missing")
        @field:Size(min = 1, message = "Amount can't be empty")
        val amount: Double,
        @field:NotNull(message = "Message can't be missing")
        @field:Size(min = 1, message = "Message can't be empty")
        val msg: String)