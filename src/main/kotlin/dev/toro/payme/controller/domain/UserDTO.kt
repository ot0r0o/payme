package dev.toro.payme.controller.domain

import dev.toro.payme.repository.entity.User
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class UserDTO(
        @field:NotNull(message = "Username can't be missing")
        @field:Size(min = 1, message = "Username can't be empty")
        val userName: String,
        @field:NotNull(message = "First name can't be missing")
        val firstName: String,
        @field:NotNull(message = "Last name can't be missing")
        val lastName: String,
        val balance: Double = 0.0,
        @field:NotNull(message = "Currency can't be missing")
        @field:Pattern(regexp = "(EUR)|(USD)")
        val currency: String,
        @field:NotNull(message = "Password can't be missing")
        @field:Size(min = 4, message = "Password can't be short")
        var password: String) {

    fun transformUser(): User {
        return User(this.userName, this.firstName, this.lastName,
                this.balance, User.Currency.valueOf(this.currency), this.password)
    }
}

