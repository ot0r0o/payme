package dev.toro.payme.controller

import dev.toro.payme.service.BalanceService
import dev.toro.payme.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * These endpoint are accessible only for ADMIN users
 */
@RestController
@RequestMapping("/admin")
//@ApiOperation(value = "", authorizations = @Authorization(value="apiKey"))
class AdminController(private val balanceService: BalanceService, private val userService: UserService) {

    /**
     * This endpoint returns the current balance of an user
     */
    @GetMapping("/b/{userName}")
    fun getBalance(@PathVariable("userName") userName: String) =
            ResponseEntity.ok().body(hashMapOf("current_balance" to balanceService.currentBalance(userName)))

    /**
     * This endpoint returns all the registered users
     */
    @GetMapping("/u/all")
    fun findAll() = userService.findAll()

}