package dev.toro.payme.controller

import dev.toro.payme.controller.domain.UserDTO
import dev.toro.payme.controller.exception.UserCreationException
import dev.toro.payme.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/user")
class UserController(val service: UserService) {

    /**
     * This endpoint creates a new user. Username is unique
     */
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Valid @RequestBody u: UserDTO) {
        if (service.createUser(u, false)){
            return
        }else {
            throw UserCreationException("Error creating user: ${u.userName}")
        }
    }

    /***
     * This endpoint edits an user.
     */
    @PostMapping("/edit")
    @ResponseStatus(HttpStatus.CREATED)
    fun edit(@Valid @RequestBody u: UserDTO) = service.editUser(u)
}