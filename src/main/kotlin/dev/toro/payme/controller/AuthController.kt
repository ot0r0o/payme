package dev.toro.payme.controller

import dev.toro.payme.controller.domain.Credentials
import org.glassfish.jersey.client.ClientConfig
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Form
import javax.ws.rs.core.MediaType


@RestController
@RequestMapping("/auth")
class AuthController {

    @Value("\${server.port}")
    private val port: String? = null

    @Value("\${security.jwt.client-id}")
    private val clientId: String? = null

    @Value("\${security.jwt.client-secret}")
    private val clientSecret: String? = null

    /**
     * This endpoint returns the JWT generated for an user and allows him to navigate through all the endpoints.
     * This JWT token should be set as an Authorization header (Authorization: Bearer $TOKEN)
     */
    @PostMapping("/token")
    fun auth(@Valid @RequestBody credentials: Credentials): ResponseEntity<String> =
            ResponseEntity.ok(getOAuthToken(credentials.userName, credentials.password))

    fun getOAuthToken(user: String, password: String) : String {
        val httpAuthenticationFeature = HttpAuthenticationFeature.basic(clientId, clientSecret)
        val clientConfig = ClientConfig()
        clientConfig.register(httpAuthenticationFeature)
        val client = ClientBuilder.newClient(clientConfig)

        val webTarget: WebTarget = client.target("http://127.0.0.1:$port/oauth/token")

        val form = Form()
        form.param("grant_type", "password")
        form.param("username", user)
        form.param("password", password)

        return webTarget.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), String::class.java)
    }


}