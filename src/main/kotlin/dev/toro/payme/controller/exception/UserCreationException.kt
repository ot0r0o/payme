package dev.toro.payme.controller.exception

class UserCreationException(override var message:String): Exception(message)