package dev.toro.payme.controller.exception

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    @ResponseBody
    fun onException(ex: Exception): ResponseEntity<ErrorResponse> {
        val httpError: HttpStatus
        val errorResponse: ErrorResponse
        when (ex) {
            is UserCreationException, is MoneyTransferException -> {
                httpError = HttpStatus.BAD_REQUEST
                errorResponse = ErrorResponse(httpError.value(), ex.message ?: "Error processing request")
            }
            is DataIntegrityViolationException -> {
                httpError = HttpStatus.BAD_REQUEST
                errorResponse = ErrorResponse(httpError.value(), "Username already exists")
            }
            is UsernameNotFoundException -> {
                httpError = HttpStatus.NOT_FOUND
                errorResponse = ErrorResponse(httpError.value(), ex.message ?: "User not found")
            }
            is InvalidGrantException, is AccessDeniedException -> {
                httpError = HttpStatus.FORBIDDEN
                errorResponse = ErrorResponse(httpError.value(), ex.message ?: "Access not granted or bad credentials")
            }
            else -> {
                logger.error(ex.message)
                httpError = HttpStatus.INTERNAL_SERVER_ERROR
                errorResponse = ErrorResponse(httpError.value(), "Unknown Error")
            }
        }

        return ResponseEntity(errorResponse, httpError)
    }

    data class ErrorResponse(val statusCode: Int, val message: String)
}