package dev.toro.payme.controller.exception

class MoneyTransferException(override var message:String): Exception(message)