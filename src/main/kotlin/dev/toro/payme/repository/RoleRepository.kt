package dev.toro.payme.repository

import dev.toro.payme.repository.entity.Role
import org.springframework.data.repository.CrudRepository


interface RoleRepository : CrudRepository<Role, Long>