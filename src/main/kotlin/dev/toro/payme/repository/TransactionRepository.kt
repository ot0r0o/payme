package dev.toro.payme.repository

import dev.toro.payme.repository.entity.Transaction
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface TransactionRepository : JpaRepository<Transaction, Long> {

    @Query("from Transaction t where t.srcId = :userId or t.dstId = :userId order by t.createdAt desc")
    fun findBySrcIdOrDstIdOrderByCreatedAtDesc(@Param("userId") userId: Long,
                                               pageRequest: Pageable): Page<Transaction>
}