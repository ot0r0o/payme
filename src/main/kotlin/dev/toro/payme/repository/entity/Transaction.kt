package dev.toro.payme.repository.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "transaction")
data class Transaction(val src: String, val srcId: Long,
                       val dst: String, val dstId: Long,
                       val amount: Double, val msg: String,
                       val createdAt: Long) {

    @Id
    @GeneratedValue
    var id: Long? = null
}