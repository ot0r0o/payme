package dev.toro.payme.repository.entity

import dev.toro.payme.controller.domain.UserDTO
import javax.persistence.*


@Entity
@Table(name = "user")
data class User(@Column(unique=true) val userName: String, val firstName: String,
                val lastName: String, val balance: Double,
                val currency: Currency, val password: String) {

    enum class Currency(val code: String) {
        EUR("€"), USD("$")
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")])
    var roles: List<Role>? = null

    fun transformUserDTO(): UserDTO {
        return UserDTO(this.userName, this.firstName, this.lastName,
                this.balance, this.currency.name, this.password)
    }
}

