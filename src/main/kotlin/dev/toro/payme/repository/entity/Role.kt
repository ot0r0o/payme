package dev.toro.payme.repository.entity

import javax.persistence.*


@Entity
@Table(name = "role")
data class Role(val roleName: String) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

}