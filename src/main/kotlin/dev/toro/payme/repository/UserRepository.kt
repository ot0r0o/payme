package dev.toro.payme.repository

import dev.toro.payme.repository.entity.User
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param


interface UserRepository : CrudRepository<User, Long> {

    fun findByUserName(userName: String) : User?

    @Modifying
    @Query("update User set balance = :balance where id = :id")
    fun setNewBalance(@Param("id") userId: Long?, @Param("balance") balance: Double?)
}