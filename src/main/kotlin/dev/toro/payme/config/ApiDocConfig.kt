package dev.toro.payme.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger.web.SecurityConfiguration
import springfox.documentation.swagger.web.SecurityConfigurationBuilder
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * Swagger configuration
 */
@Configuration
@EnableSwagger2
class ApiDocConfig(private val passwordEncoder: BCryptPasswordEncoder) {

    @Value("\${security.jwt.client-id}")
    private val clientId: String? = null

    @Value("\${security.jwt.client-secret}")
    private val clientSecret: String? = null

    @Value("\${security.security-realm}")
    private val securityRealm: String? = null

    @Bean
    fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("dev.toro.payme.controller"))
            .paths(PathSelectors.any())
            .build()
            .securitySchemes(listOf(apiKey()))
            .securityContexts(listOf(securityContext()))
            .apiInfo(apiInfo())

    private fun apiInfo(): ApiInfo =
            ApiInfoBuilder()
                    .title("PAY.ME Server")
                    .description("Verse Test [Docker+SpringBoot+JWT+Kotlin]")
                    .contact(Contact("Alberto Toro", "", "toro.alberto@gmail.com"))
                    .version("0.1")
                    .build()

    private fun apiKey() = ApiKey("apiKey", "Authorization", "header")

    private fun securityContext(): SecurityContext {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build()
    }

    private fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope
        return listOf(SecurityReference("apiKey", authorizationScopes))
    }

    @Bean
    fun security(): SecurityConfiguration {
        return SecurityConfigurationBuilder.builder()
                .clientId(clientId)
                .clientSecret(passwordEncoder.encode(clientSecret))
                .realm(securityRealm)
                .appName("PAY.ME")
                .scopeSeparator(",")
                .additionalQueryStringParams(null)
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build()
    }
}
