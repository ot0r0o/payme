package dev.toro.payme.service

import dev.toro.payme.controller.exception.MoneyTransferException
import dev.toro.payme.repository.TransactionRepository
import dev.toro.payme.repository.UserRepository
import dev.toro.payme.repository.entity.Transaction
import dev.toro.payme.repository.entity.User
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class BalanceService(private val userRepository: UserRepository,
                     private val txRepository: TransactionRepository) {

    val logger = LoggerFactory.getLogger(BalanceService::class.java)!!

    @Value("\${currency.rate.usd-eur}")
    private val rate: Double? = null

    /**
     * This function sends money from a source user to a destination one.
     * Before send it, checks if there is enough money and applies exchange currency if it is needed.
     */
    @Transactional
    @Throws(UsernameNotFoundException::class, MoneyTransferException::class)
    fun transfer(src : String, dst: String, amount: Double, msg: String): String {
        val srcUser: User? = userRepository.findByUserName(src)
        if (srcUser == null){
            logger.error("Could not find source user $src")
            throw UsernameNotFoundException("Could not find source user $src")
        }
        val dstUser: User? = userRepository.findByUserName(dst)
        if (dstUser == null){
            logger.error("Could not find source user $src")
            throw UsernameNotFoundException("Could not find source user $src")
        }

        //Check balance
        if (srcUser.balance < amount) {
            logger.error("ID ${srcUser.id} does not have enough balance dst transfer dst ID ${dstUser.id}")
            throw  MoneyTransferException("Not enough balance destinationUser transfer money")
        }

        val newBalance = srcUser.balance - amount
        userRepository.setNewBalance(srcUser.id, newBalance)
        userRepository.setNewBalance(dstUser.id, dstUser.balance +
                exchange(srcUser.currency, dstUser.currency, amount))
        txRepository.save(Transaction(src, srcUser.id!!, dst, dstUser.id!!, amount, msg,
                Instant.now().toEpochMilli()))

        return "$newBalance${srcUser.currency.code}"
    }

    /**
     * This function returns the current balance of an user
     */
    fun currentBalance(userName: String): String {
        val user: User = userRepository.findByUserName(userName)
                ?: throw UsernameNotFoundException("Could not find user $userName")

        return "${user.balance}${user.currency.code}"
    }

    /**
     * This function adds money to an user's balance
     */
    @Transactional
    @Throws(UsernameNotFoundException::class, MoneyTransferException::class)
    fun addMoney(userName: String, money: Int): String {
        val user: User = userRepository.findByUserName(userName)
                ?: throw UsernameNotFoundException("Could not find user $userName")
        if (money < 0){
            throw MoneyTransferException("Minimum balance to add must be 1")
        }
        val newBalance = user.balance + money
        userRepository.setNewBalance(user.id, newBalance)
        txRepository.save(Transaction(userName, user.id!!, userName, user.id!!, money.toDouble(),
                "Balance increased $money${user.currency.code}",
                Instant.now().toEpochMilli()))

        return "$newBalance${user.currency.code}"
    }

    /**
     * This function returns the paginated list of movements
     */
    @Throws(UsernameNotFoundException::class)
    fun getTransactions(userName: String, page: PageRequest): Page<Transaction> {
        val user: User = userRepository.findByUserName(userName)
                ?: throw UsernameNotFoundException("Could not find user $userName")

        return txRepository.findBySrcIdOrDstIdOrderByCreatedAtDesc(user.id!!, page)
    }

    /**
     * This function applies the current exchange
     * This rate exchange is set in the configuration file, for future solutions it should be
     * obtained from an external service
     */
    private fun exchange(srcCurrency: User.Currency, dstCurrency: User.Currency, amount: Double): Double =
        if(srcCurrency == User.Currency.EUR && dstCurrency == User.Currency.USD){
            amount / rate!!
        }else if(srcCurrency == User.Currency.USD && dstCurrency == User.Currency.EUR){
            amount * rate!!
        }else{
            amount
        }
}