package dev.toro.payme.service

import dev.toro.payme.controller.domain.UserDTO
import dev.toro.payme.repository.RoleRepository
import dev.toro.payme.repository.UserRepository
import dev.toro.payme.repository.entity.Role
import dev.toro.payme.repository.entity.User
import org.slf4j.LoggerFactory
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserService(private val repository: UserRepository,
                  private val roleRepository: RoleRepository): UserDetailsService {

    val logger = LoggerFactory.getLogger(UserService::class.java)!!

    override fun loadUserByUsername(userName: String): UserDetails {
        val user = findByUserName(userName)
        val authorities = ArrayList<GrantedAuthority>()
        user.roles!!.forEach { role -> authorities.add(SimpleGrantedAuthority(role.roleName)) }

        logger.info("User {${user.id}} logged in")
        return org.springframework.security.core.userdetails.User(user.userName, user.password, authorities)
    }

    /**
     * This function returns an user based on his username
     */
    @Throws(UsernameNotFoundException::class)
    fun findByUserName(userName: String): User =
            repository.findByUserName(userName)
                    ?: throw UsernameNotFoundException("Could not find user $userName")

    /**
     * This function receives an UserDTO in order to create a new User
     */
    @Transactional
    fun createUser(newUser : UserDTO, admin: Boolean): Boolean {
        logger.debug("New user: ${newUser.userName}")
        newUser.password = BCryptPasswordEncoder().encode(newUser.password)
        val u: User = newUser.transformUser()
        u.roles = mutableListOf(
                if (admin) Role("ADMIN_USER") else Role("STANDARD_USER")
        )
        roleRepository.saveAll((u.roles as MutableList<Role>))
        return (repository.save(u).id != -1L)
    }

    /**
     * This function edits an existing user
     */
    @Transactional
    @Throws(UsernameNotFoundException::class)
    fun editUser(userDTO: UserDTO): User {
        logger.debug("Editing user: ${userDTO.userName}")
        val user = repository.findByUserName(userDTO.userName)
                ?: throw UsernameNotFoundException("Could not find user ${userDTO.userName}")
        val editedUser = userDTO.transformUser()
        editedUser.id = user.id
        return repository.save(editedUser)
    }

    /**
     * This function return all the users registered in the service
     */
    fun findAll(): List<String> =
            repository.findAll().map { user -> user.userName }
}