package dev.toro.payme

import dev.toro.payme.repository.RoleRepository
import dev.toro.payme.repository.UserRepository
import dev.toro.payme.repository.entity.Role
import dev.toro.payme.repository.entity.User
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class PaymeApplication {

    private val logger = LoggerFactory.getLogger(PaymeApplication::class.java)

    @Bean
    fun init(userRepository: UserRepository, roleRepository: RoleRepository) = CommandLineRunner {
        // create test users
        val rUser = Role("STANDARD_USER")
        val rAdmin = Role("ADMIN_USER")
        roleRepository.saveAll(mutableListOf(rAdmin, rUser))

        //P: 1234
        val u1 = User("user1", "John", "Doe", 10.0, User.Currency.EUR, "\$2a\$10\$D1G08Q9sBEQPLxf07xdVy.sU4oI8ecU06VXA5PqYNRW3vTqe36mj2")
        u1.roles = listOf(rAdmin, rUser)
        //P: 4321
        val u2 = User("user2", "Bob", "Alice", 30.0, User.Currency.USD, "\$2a\$10\$Dx.dfuN9iNS/u/ojAo26TuDmZhRs7KTDoawShrJ8mdpdMP88pvl5G")
        u2.roles = listOf(rUser)
        userRepository.saveAll(mutableListOf(u1, u2))

        // check test users
        logger.debug("Check new users:")
        userRepository.findAll().forEach { logger.debug(it.toString()) }
    }
}

fun main(args: Array<String>) {
    runApplication<PaymeApplication>(*args)
}
